export const teams = [
  { color: "#3F51B5", name: "Синия", win: "синих" },
  { color: "#FE7043", name: "Ораженвая", win: "оранжевых" },
  { color: "#33691E", name: "Зеленая", win: "зеленых" },
  { color: "#4A148C", name: "Фиолетовая", win: "фиолетвых" }
];

// views
export const NEW_GAME = "NEW_GAME";
export const MAIN = "MAIN";
export const START_TURN = "START_TURN";
export const TURN = "TURN";
export const RULES = "RULES";
export const END_TURN = "END_TURN";
export const END_ROUND = "END_ROUND";

export const TURN_TIME = 30000; // 30000 -> 30 sec

// rounds
export const WORD = "WORD";
export const GESTURE = "GESTURE";
export const ONE_WORD = "ONE_WORD";

export const ROUND_WORDING = {
  [WORD]: "НОВАЯ ИГРА",
  [GESTURE]: "ЖЕСТЫ",
  [ONE_WORD]: "ОДНО СЛОВО"
};
