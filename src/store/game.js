import { writable, get, derived } from "svelte/store";
import {
  teams as teamsList,
  MAIN,
  WORD,
  GESTURE,
  ONE_WORD
} from "../constants/index.js";

import wordDictionary from "../constants/words.js";

export const wordCount = (() => {
  const { subscribe, update, set } = writable(24);
  return {
    subscribe,
    add: () => update(state => (state += 1)),
    substruct: () => update(state => (state -= 1)),
    reset: () => set(24)
  };
})();

let usedWords = {};

export const words = (() => {
  const { subscribe, update, set } = writable([]);
  return {
    subscribe,
    newGame: () => {
      if (
        Object.keys(usedWords).length + get(wordCount) >=
        wordDictionary.length
      ) {
        usedWords = {};
      }

      const wordsForRound = wordDictionary
        .filter(item => !usedWords[item.name]) // remove used words
        .sort(() => 0.5 - Math.random()) // shuffle words
        .slice(0, get(wordCount)) // slice word count
        .map(item => ({ ...item })); // copy to new array
      wordsForRound.forEach(item => (usedWords[item.name] = true)); // add words to used

      set(wordsForRound);
    },

    guess: word =>
      update(state => {
        state.find(item => item.name === word).guessed = true;
        const [firstWord, ...rest] = state;
        return [...rest, firstWord];
      }),
    endTurn: () =>
      update(state => {
        const [firstWord, ...rest] = state;
        return [...rest, firstWord];
      }),
    nextRound: () =>
      update(state =>
        state
          .sort(function() {
            return 0.5 - Math.random();
          })
          .map(word => ({ ...word, guessed: false }))
      )
  };
})();

export const stack = derived(words, $words =>
  $words.filter(word => !word.guessed)
);

export const teams = (() => {
  const { subscribe, update } = writable({
    active: 0,
    activeTeamScore: 0,
    list: teamsList.slice(0, 2)
  });
  return {
    subscribe,
    add: () =>
      update(state => ({
        ...state,
        list: teamsList.slice(0, state.list.length + 1)
      })),
    substruct: () =>
      update(state => ({
        ...state,
        list: teamsList.slice(0, state.list.length - 1)
      })),
    newGame: () =>
      update(state => ({
        ...state,
        list: state.list.map(team => ({ ...team, score: 0, wins: 0 })),
        activeTeamScore: 0
      })),
    nextTeam: () =>
      update(state => ({
        ...state,
        active: state.list.length - 1 === state.active ? 0 : state.active + 1,
        activeTeamScore: 0
      })),
    guess: () =>
      update(state => {
        state.activeTeamScore += 1;
        state.list[state.active].score =
          (state.list[state.active].score || 0) + 1;
        return state;
      }),
    nextRound: () =>
      update(state => {
        const winScore = state.list.reduce(
          (res, team) => (team.score > res ? team.score : res),
          0
        );
        state.list = state.list.map(team => ({
          ...team,
          score: 0,
          wins: team.score === winScore ? team.wins + 1 : team.wins
        }));
        state.activeTeamScore = 0;
        return state;
      })
  };
})();

export const activeTeam = derived(teams, $teams => $teams.list[$teams.active]);

export const view = writable(MAIN);

export const round = (() => {
  const { subscribe, update, set } = writable(WORD);
  return {
    subscribe,
    next: () =>
      update(state => {
        if (state === WORD) return GESTURE;
        if (state === GESTURE) return ONE_WORD;
        return WORD;
      }),
    reset: () => set(WORD)
  };
})();

export const isModalOpen = writable(false);
